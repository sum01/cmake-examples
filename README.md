# Cmake examples

These are a collection of minimalist examples for handling different situations with [Cmake](https://cmake.org/)  
Each folder is a self-contained Cmake project that tries to explain the best practices for "modern" Cmake.

If you have any questions, read the comments in whichever CMakeLists.txt file you're interested in.

## Dependencies

[Cmake](https://cmake.org/) is always needed for building (v3 or newer).

Some projects will require different Cmake versions, and sometimes a specific C++ standard.  
This will be specified in their respective README's if required.

## Build & Install

These are some common commands that can be used to build/install a Cmake project.  
If you're wondering what the `-D` means, it's telling Cmake to set the passed command+value.

**Notes:**

* `BUILD_TESTING`, while common, is not guaranteed to prevent building tests.  
  Read a project's documentation to make sure it'll actually work, as some use different variables.
* If you don't want to install, remove `--target install` from the last command.  
  If you do that, you can also run it as unprivledged (i.e. no `sudo`)
* Creating a `build` dir and building from it is optional, but recommended.  
  This keeps build files out of the main files.

Linux/macOS:

```
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF ..
sudo cmake --build . --target install
```

Windows:

```
mkdir build
cd build
cmake -DBUILD_TESTING=OFF ..
runas /user:Administrator "cmake --build . --config Release --target install"
```
