# Using a Cmake-enabled library

This is a simple example of how to create a binary that depends on a Cmake-enabled library.

We'll be using [GLFW](https://github.com/glfw/glfw), but any library that uses Cmake could be used the same way.

If you are curious about anything, read the comments in the CMakeLists.txt  
If everything goes as planned, the executable prints a "GLFW was found :)" message when run.

## Libraries As Submodules

The GLFW library is added as a Git submodule.  
This is done by running the following...

```shell
git submodule add https://github.com/glfw/glfw.git glfw
git submodule foreach --recursive 'git fetch --tags'
cd glfw
git checkout 3.2.1
```

Then when the submodule is actually added and commited with Git, it will be on the 3.2.1 tag, instead of a random commit.  
This is so you can target a specific tag for stability.
