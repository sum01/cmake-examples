// Note that even though it's in <>
// it will still work since CMake can...
// tell it where to look for includes
#include <GLFW/glfw3.h>
// For EXIT_XXX
#include <cstdlib>
#include <iostream>

int main() {
	// This is defined in the glfw3.h file
	#ifdef _glfw3_h_
		std::cout << "GLFW was found :)" << std::endl;
	#else
		std::cerr << "GLFW wasn't found :(" << std::endl;
		return EXIT_FAILURE;
	#endif
	return EXIT_SUCCESS;
}
